﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logical_Programs
{
    public class LogicalPrograms
    {
        public void CountNoOfOnes()
        {
            int Number = GetIntNumber();
            int count = 0;
            void abc()
            {
                Console.WriteLine("One   ");
            }
            while (Number > 0)
            {
                if ((Number % 10) == 1)
                    count++;
                Number /= 10;
            }
            Console.WriteLine("Number of ones ={0}", count);
            abc();
        }

        internal void JaggedArray()
        {
           int[][] num =new int[4][];
            num[0] = new int[4];
            num[1] = new int[6];
            num[2] = new int[2];
            num[3] = new int[8];
            

            int x = 10;
            for (int i = 0; i < num.GetLength(0); i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    num[i][j] = x++;
                }
            }

            Console.WriteLine();
            Console.WriteLine("Accessing elements form Jagged array using foreach loop");
            for (int i = 0; i < num.GetLength(0); i++)
            {
                foreach (var item in num[i])
                {
                    Console.Write(item + " ");
                }
            }


            Console.WriteLine();
            Console.WriteLine("Accessing elements form Jagged array using nested for loop");

            for (int i = 0; i < num.GetLength(0); i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    Console.Write(num[i][j]+" ");
                }
                Console.WriteLine();
            }

        }

        internal void TowDArray()
        {
            int[,] arr = new int[4, 5];

            int x = 10;

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = x++;                    
                }
            }

            Console.WriteLine("Accessing values using foreach");
            foreach (var item in arr)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Accessing values using Nested for loop");
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j]+" ");
                }
                Console.WriteLine();
            }
        }

        internal void OneDArraywithFunctions()
        {
            int[] arr = new int[5];
            int x = 5;
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = x;
                x += 5;               
            }

            foreach (var item in arr)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Sorted Array");
            Array.Sort(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Reverse Array");
            Array.Reverse(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]+" ");
            }
            Console.WriteLine();
            Console.WriteLine("Copy Array");
            int[] copy = new int[5];
            Array.Copy(arr, copy,5);

            foreach (var item in copy)
            {
                Console.Write(item + " ");
            }
        }

        internal void OneDArray()
        {
            int[] array = {10,20,30,40,50};
            int x = 5;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i]+" ");
            }
            Console.WriteLine();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = x;
                x += 5;
            }
            Console.WriteLine();
            foreach (var item in array)
            {
                Console.Write(item + " ");
            }
             


        }

        public int GetIntNumber()
        {
            Console.WriteLine("Enter number");
            return Convert.ToInt32(Console.ReadLine());
        }

        public void BinaryTriangle()
        {
            int Limit = GetIntNumber();
            int temp = 1;
            for (int i = 0; i <= Limit; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(temp);
                    temp = (temp == 1 ? 0 : 1);
                }
                Console.WriteLine();
            }
        }

        public void NumberTocharacter()
        {
            int Num = GetIntNumber();
            string NumInString = string.Empty;
            List<string> StringNumbers = new List<string>();
            while (Num > 0)
            {

                DigitToChar(Num % 10, out NumInString);

                StringNumbers.Add(NumInString);

                Num /= 10;
            }

            for (int i = StringNumbers.Count - 1; i >= 0; i--)
            {
                Console.Write(StringNumbers[i] + " ");
            }
        }

        private void DigitToChar(int num, out string NumInString)
        {
            switch (num)
            {
                case 0: NumInString = "Zero"; break;
                case 1: NumInString = "One"; break;
                case 2: NumInString = "Two"; break;
                case 3: NumInString = "Three"; break;
                case 4: NumInString = "Four"; break;
                case 5: NumInString = "Five"; break;
                case 6: NumInString = "Six"; break;
                case 7: NumInString = "Seven"; break;
                case 8: NumInString = "Eight"; break;
                case 9: NumInString = "Nine"; break;
                default: NumInString = ""; break;
            }
        }



        public void FibonacciSeriesPattren()
        {
            int limit = GetIntNumber();
            int x, y, z;

            for (int i = 1; i <= limit; i++)
            {
                x = 1; y = 0; z = 1;
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(z + " ");
                    x = y;
                    y = z;
                    z = x + y;
                }
                Console.WriteLine();
            }
        }

        public void Pattren4()
        {
            int limit = GetIntNumber();
            for (int i = 1; i < limit; i++)
            {

                for (int k = 1; k < limit - i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <=i; j++)
                {
                    Console.Write(j);
                }
                for (int j = i - 1; j >=1; j--)
                {
                    Console.Write(j);
                }
                Console.WriteLine();
            }

        }

        public void Pattren3()
        {
            int limit = GetIntNumber();
            for (int i = 1; i <=limit; i++)
            {
                for (int k = 1; k <=limit-i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <=i; j++)
                {
                    Console.Write(Convert.ToChar(64+j));
                }

                for (int j = i - 1; j >= 1; j--) { 
                    Console.Write(Convert.ToChar(64 + j));
                }
                Console.WriteLine();
            }

            for (int i =limit-1; i >=1; i--)
            {
                for (int k = 1; k <=limit - i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(Convert.ToChar(64 + j));
                }

                for (int j = i-1; j >= 1; j--)
                {
                    Console.Write(Convert.ToChar(64 + j));
                }
                Console.WriteLine();
            }
        }

        public void Pattren2()
        {
            char ch = 'A';
            int limit = GetIntNumber();
            for (int i = 1; i <= limit; i++)
            {
                for (int j = 1; j <= i; j++)
                    Console.Write(ch);
                ch++;
                Console.WriteLine();
            }
        }
        public void Pattren1()
        {
            int limit = GetIntNumber();
            char ch;
            for (int i = 1; i <= limit; i++)
            {
                for (int k = limit; k > i; k--)
                    Console.Write(" ");

                ch = 'A';
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(ch++);
                }

                ch--;
                for (int j = 1; j <= i - 1; j++)
                {
                    Console.Write(--ch);
                }

                Console.WriteLine();
            }
        }

        public void NumberTriangel1()
        {
            int limit = GetIntNumber();
            int k = 1;
            for (int i = 1; i <= limit; i++)
            {

                for (int j = 1; j <= i; j++, k++)
                    Console.Write(k + " ");

                Console.WriteLine();
            }
        }
        public void Dimand()
        {
            int limit = GetIntNumber();

            for (int i = 1; i <= limit; i++)
            {
                for (int k = limit - 1; k >= i; k--)
                    Console.Write(" ");
                for (int j = 1; j <= i; j++)
                    Console.Write("*");
                for (int j = 1; j <= i - 1; j++)
                    Console.Write("*");
                Console.WriteLine();
            }
            for (int i = limit - 1; i >= 1; i--)
            {
                for (int k = limit - 1; k >= i; k--)
                    Console.Write(" ");
                for (int j = 1; j <= i; j++)
                    Console.Write("*");
                for (int j = 1; j <= i - 1; j++)
                    Console.Write("*");
                Console.WriteLine();
            }
        }
    }
}
